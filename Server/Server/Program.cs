﻿using System;

namespace CS {
    class Program {
        static void Main(string[] args) {
            var server = new MyServer();
            server.Run();

            Console.ReadKey();
        }
    }
}