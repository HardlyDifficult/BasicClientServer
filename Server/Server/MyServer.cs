﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace CS {
    public class MyServer {
        int connectionCounter;
        TcpListener listener;

        public MyServer() {
            listener = new TcpListener(IPAddress.Any, CSData.port);
        }

        public void Run() {
            try {
                listener.Start();
                while(true) {
                    AcceptAndHandleNewConnection();
                }
            } catch(Exception e) {
                Console.WriteLine($"Server failed: {e}");
            }
        }

        private void AcceptAndHandleNewConnection() {
            using(var socket = listener.AcceptSocket()) {
                HandleConnection(socket);
            }
        }

        private void HandleConnection(Socket socket) {
            connectionCounter++;

            using(var networkStream = new NetworkStream(socket)) {
                try {
                    while(true) {
                        ReadAndRespondToNextLine(socket, networkStream);
                    }
                } catch(Exception e) {
                    Console.WriteLine($"Client closed connection: {e}");
                }
            }
        }

        private void ReadAndRespondToNextLine(Socket socket, NetworkStream networkStream) {
            var reader = new StreamReader(networkStream);
            var message = reader.ReadLine();
            Console.WriteLine($"Client connection, they said: {message}");

            using(var memory = new MemoryStream()) {
                using(var writer = new StreamWriter(memory)) {
                    writer.WriteLine($"Hello, you are connection number {connectionCounter} ... and you said {message}");
                }
                socket.Send(memory.ToArray());
            }
        }
    }
}