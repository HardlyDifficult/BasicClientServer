﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace CS {
    public class MyClient : IDisposable {
        TcpClient client;
        StreamReader reader;
        NetworkStream stream;
        StreamWriter writer;

        public MyClient() {
            Reconnect();
        }

        void IDisposable.Dispose() {
            stream.Dispose();
            client.Close();
        }

        public void SendAndReceiveSomething(string clientMessage) {
            while(true) {
                try {
                    writer.WriteLine(clientMessage);
                    writer.Flush();

                    var message = reader.ReadLine();
                    Console.WriteLine(message);
                    break;
                } catch(Exception e) {
                    // Console.WriteLine($"Server connection issue, attempting reconnection in 1s: {e}");
                    Thread.Sleep(1000);

                    Reconnect();
                }
            }
        }

        private void Reconnect() {
            try {
                if(stream != null) {
                    stream.Dispose();
                }
                if(client != null) {
                    client.Close();
                }

                client = new TcpClient("localhost", CSData.port);
                stream = client.GetStream();
                writer = new StreamWriter(client.GetStream());
                reader = new StreamReader(client.GetStream());
            } catch(Exception e) {
                //Console.WriteLine($"Connect failed...: {e2}");
            }
        }
    }
}