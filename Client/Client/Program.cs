﻿using System;

namespace CS {
    class Program {
        static void Main(string[] args) {
            using(var client = new MyClient()) {
                while(true) {
                    var key = Console.ReadKey().Key;
                    if(key == ConsoleKey.Escape) {
                        break;
                    }

                    client.SendAndReceiveSomething(key.ToString());
                }
            }
        }
    }
}